package com.cy.pj.goods;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Repository//此诸结一般用于描述数据逻辑曾的具体实现对象，此对象要交给spring管理
public class DefaultGoodsDao implements GoodsDao{
    @Autowired
    private DataSource dataSource;

    /**
     * 对于如下如下方法的实现，你觉得？
     * 哪些地方比较固定
     * 哪些地方写的比较死板，不够灵活
     * @return
     */
    @Override
    public  List<Map<String, Object>> findGoods() {
        /**
         * 获取jdbc
         * 创建statement对象，基于此对象发送Sql
         * 发送SQL，执行查询
         * 处理结果集（将所有记录封装到List<Map<String,Object>>）
         * 释放资源
         */
        Connection connection=null;
        Statement statement=null;
        ResultSet rs=null;
        String sql="select * from tb_goods";
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            rs = statement.executeQuery(sql);
            List<Map<String, Object>> list = new ArrayList<>();
            ResultSetMetaData resultSetMetaData = rs.getMetaData();//获取结果集中的源数据
           while (rs.next()) {

//                map.put("id", rs.getInt("id"));
//                map.put("name", rs.getString("name"));
//                map.put("remark", rs.getString("remark"));
//                map.put("createdTime", rs.getTimestamp("createdTime"));
//                list.add(map);
               list.add(rowMap(resultSetMetaData,rs));
            }
            return list;
        }catch (SQLException e){
            e.printStackTrace();
            //return null;
            throw new RuntimeException(e);//非检查异常，这样方法就不需要声明抛出了
        }finally {
           close(rs,statement,connection);
        }

    }
    //行映射
    public Map<String,Object> rowMap(ResultSetMetaData resultSetMetaData,ResultSet rs)
            throws SQLException {
        Map<String, Object> map = new HashMap<>();
        //获取列的数量
        int columnCount = resultSetMetaData.getColumnCount();
        for (int i = 1; i <=columnCount ; i++) {
            //获取列别名，没有起别名就是列的名字
            String columnLabel = resultSetMetaData.getColumnLabel(i);
            //将列的名字作为key，列的值作为value存储到map集合
            map.put(columnLabel,rs.getObject(columnLabel));
        }return map;
    }
    public void close(ResultSet rs,Statement statement,Connection connection){
        if (rs!=null)
            try {
                rs.close();
            }catch (SQLException e){e.printStackTrace();}
        if (statement!=null)
            try {
                statement.close();
            }catch (SQLException e){e.printStackTrace();}
        if (connection!=null)
            try {
                connection.close();
            }catch (SQLException e){e.printStackTrace();}
    }
}
