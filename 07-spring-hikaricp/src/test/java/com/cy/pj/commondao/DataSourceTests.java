package com.cy.pj.commondao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
public class DataSourceTests {
    /**
     * DataSource为java中定义的一个连接池规范
     * 所有连接池产品中必须定义一个这个规范的实现
     */
    @Autowired
    private DataSource dataSource;//请问这个dataSource指向的具体对象是谁
    @Test
    void testGetConnection() throws SQLException {
        //获取dataSource变量指向的对象的具体类型的名字
        //com.zaxxer.hikari.HikariDataSource
        System.out.println(dataSource.getClass().getName());
        //请问，通过dataSource获取连接的大概过程是怎样的？
        //通过dataSource获取连接池（连接池不存在时则创建池）-HikariPool->CopyOnWriteArrayList
        //底层基于jdbc获取与数据库的连接，并将连接存储池中
        //返回池中连接
        Connection conn = dataSource.getConnection();
        System.out.println(conn);
    }
}
