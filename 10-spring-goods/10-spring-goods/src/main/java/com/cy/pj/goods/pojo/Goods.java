package com.cy.pj.goods.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@AllArgsConstructor//有参构造函数
@NoArgsConstructor//无参构造函数
@Data//此注解描述类时会自动生成get set方法
public class Goods {
    private Integer id;
    private String name;
    private String remark;
    //alt+enter 引入类
    private Date createdTime;
    //alt+insert 添加方法

    }


