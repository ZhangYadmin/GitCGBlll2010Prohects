package com.cy.pj.goods.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//@ResponseBody
@Slf4j
//@ControllerAdvice//此注解描述的类为全局异常处理类
@RestControllerAdvice
public class GlobalExceptionHandler {

//    @ExceptionHandler(IllegalArgumentException.class)//描述的为一个controller中的异常处理方法 括号里为当前方法可以处理的异常类型
//
//    public String doHandleRuntimeException(IllegalArgumentException e){
//        log.error("IllegalArgumentException.exception{}",e.getMessage());
//        return e.getMessage();
//    }



//    @ExceptionHandler(RuntimeException.class)//描述的为一个controller中的异常处理方法 括号里为当前方法可以处理的异常类型
//
//    public String doHandleRuntimeException(RuntimeException e){
//        log.error("GoodsController.exception{}",e.getMessage());
//        return e.getMessage();
//    }

    @ExceptionHandler(RuntimeException.class)//描述的为一个controller中的异常处理方法 括号里为当前方法可以处理的异常类型

    public JsonResult doHandleRuntimeException(RuntimeException e){
        log.error("GoodsController.exception{}",e.getMessage());
        return new JsonResult(e);
    }

}
