package com.cy.pj.goods.service;

import com.cy.pj.goods.pojo.Goods;

import java.util.List;

public interface GoodsService {
    int saveGoods(Goods goods);
    List<Goods> findGoods();

    int deleteById(Integer id);

    Goods findById(Integer id);

    int updateGoods(Goods goods);

    List<Goods> findGoods(String name);
}
