package com.cy.pj.goods.controller;

import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/goods/")
@Slf4j
public class JsonGoodsController {
    @Autowired
    private GoodsService goodsService;

//    @ExceptionHandler(RuntimeException.class)//描述的为一个controller中的异常处理方法 括号里为当前方法可以处理的异常类型
//    @ResponseBody
//    public String doHandleRuntimeException(RuntimeException e){
//        log.error("exception{}",e.getMessage());
//        return e.getMessage();
//    }
//基于条件查询商品信息，并将信息转换为json串返回
    @GetMapping("doFindJsonGoods")
    @ResponseBody
    public JsonResult doFindJSONGOODS(String name){
        List<Goods> list = goodsService.findGoods(name);
        return new JsonResult(list);
    }

    @GetMapping("doFindById/{id}")
    public String doFindById(@PathVariable Integer id, Model model){
        Goods goods=goodsService.findById(id);
        model.addAttribute("goods", goods);
        return "goods-update";
    }
    @PostMapping("doUpdateGoods")
    public String doUpdateGoods(Goods goods,Model model){
        goodsService.updateGoods(goods);
        List<Goods> list = goodsService.findGoods(null);
        model.addAttribute("list", list);
        return "goods";
    }

    @RequestMapping("doDeleteById")
    public String doDeleteById(Integer id,Model model){
        goodsService.deleteById(id);
        List<Goods> list=goodsService.findGoods(null);
        model.addAttribute("list", list);
        return "goods";
    }
    @PostMapping("doSaveGoods")
    public String doSaveGoods(Goods goods,Model model){
        goodsService.saveGoods(goods);
        List<Goods> list = goodsService.findGoods(null);
        model.addAttribute("list",list);
        return "goods";
    }


    @GetMapping("doGoodsAddUI")
    public String doGoodsAddUI(){
        return "goods-add";
    }


    @GetMapping("doFindGoods")
    public String doFindGoods(String name,Model model){
        List<Goods> list = goodsService.findGoods(name);

        model.addAttribute("list",list);
        return "goods";
    }

    @PostMapping("doSaveJsonGoods")
    public JsonResult doSaveGoods(@RequestBody Goods goods){
        goodsService.saveGoods(goods);
        return new JsonResult("save ok");
    }

}
