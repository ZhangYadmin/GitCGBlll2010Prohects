function ajax(jsonObj){//jsonObj对象格式有要求
    //1.create xhr
    let xhr=createXHR(jsonObj.callback);
    //2.create connection
    let requestUrl=jsonObj.requestUrl;
    let requestParams=jsonObj.requestParams;
    let requestType=jsonObj.requestMethod;
    let url=requestType=="GET"?`${requestUrl}/${requestParams}`:requestUrl;
    xhr.open(requestType,url,true);
    if(requestType=="POST"){
        xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    }
    //3.send request
    xhr.send(requestType=="POST"?requestParams:null);
}

function ajaxGet(url, params, callback) {
    //创建XHR对象
    let xhr=creatXHR(callback);
    //建立连接
    xhr.open("GET",`${url}/${params}`,true);
    //send request
    xhr.send();
}
function ajaxPost(url, params, callback) {
    //创建XHR对象
    let xhr=creatXHR(callback);
    //建立连接
    xhr.open("POST",url,true);
    //send request
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send(params);
}

function ajaxPostJson(url,params,callback){//封装ajax get 请求模板代码
    //1.create xhr and set onreadystate change
    let xhr=creatXHR(callback);
    //2.open connection
    xhr.open("POST",url,true);
    xhr.setRequestHeader("Content-Type","application/json");
    //3.send request
    xhr.send(JSON.stringify(params));//将json对象转换为json格式字符串提交到服务端
}
function ajaxPut(url, params, callback) {
    //创建XHR对象
    let xhr=creatXHR(callback);
    //建立连接
    xhr.open("put",url,true);
    //send request
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send(params);
}
function ajaxDelete(url, params, callback) {
    //创建XHR对象
    let xhr=creatXHR(callback);
    //建立连接
    xhr.open("delete",`${url}/${params}`,true);
    //send request
    xhr.send();
}


function creatXHR(callback) {
    //创建XHR对象
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                callback(xhr.responseText);
            }
        }
    }
    return xhr;
}