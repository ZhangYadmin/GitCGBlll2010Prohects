package com.cy.common.cacha;

import com.cy.common.DefaulfCache;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DefaultCacheTests {
    //spring中可以借助@Autowired注解描述属性，用于告诉spring这个属性的值由spring注入
    //DefaultCacheTests 类中有一个DefaulfCache类型的属性
    @Autowired
    private DefaulfCache defaultCache;//spring 会对对其进行DI操作

    @Test
    void testDefaultCahce() {
        System.out.println(defaultCache.toString());
    }

    @Autowired
    private BeanFactory beanFactory;

    @Test
    void testGetBean() {
        //基于工厂获取bean对象
        //这个bean对象在哪呢（有可能在Bean池中-Map<String,Object>）
        System.out.println(beanFactory.getBean("defaulfCache"));

    }
}
