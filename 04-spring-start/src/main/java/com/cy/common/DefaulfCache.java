package com.cy.common;

import org.springframework.stereotype.Component;

/**
 * @Component 注解为spring框架中定义的用于描述一般bean对象的
 * 一个注解
 * springbott工程在启动时会自动扫描启动类所在包以及子包中
 * 的类，并读取这些类的描述，加入这些类使用了类似@Component
 * 这样的注解描述，他们会交给spring管理
 */
@Component
public class DefaulfCache {
}
