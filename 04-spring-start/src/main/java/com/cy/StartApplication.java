package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.io.ResourceLoader;

/**
 * @SpringBootApplication注解描述的类为springboot工程启动类 这个启动类会通过@SpringBootApplication 注解告诉springboot工程
 * 项目在启动时要做那些基础操作
 * 基于线程调用i/o从磁盘读取类，将其加载到内存，此时会基于类
 * 创建字节码对象（其类型为Class类型）
 * 基于Class（字节码对象）读取类的配置信息（@Component）
 * 基于类的配置进行相应的配置存储（交给spring管理）-Map<String,BeanDefinition>
 * 基于类的配置借助BeanFactory创建类的实例，多个对象存储到Map<String, Object>
 */
@SpringBootApplication
public class StartApplication {

    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class, args);
    }

}
