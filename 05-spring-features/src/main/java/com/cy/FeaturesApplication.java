package com.cy;

import com.cy.common.pool.ObjectPool;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FeaturesApplication {
@Bean
public ObjectPool objectPool(){
    return  new ObjectPool();
}
    public static void main(String[] args) {
        SpringApplication.run(FeaturesApplication.class, args);
    }

}
