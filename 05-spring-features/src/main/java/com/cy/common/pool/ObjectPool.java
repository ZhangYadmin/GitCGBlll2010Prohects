package com.cy.common.pool;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 項目的啓動類在啓動是會將此類加載到内存吗
 * 会，但是有条件，包结构必须正确，可以通过JVM参数检测类是否被加载了 -XX：+TraceClassLoading
 * 项目的启动类在启动会默认创建objectpool类型的实例吗？
 * 会的，可以通过构造方法进行效验
 * 对于一个池对象而言，相对与普通对象占用的资源是多还是少？
 * 多,但这个对象暂时不使用，又占用很多的资源是否会影响性能？
 *
 */

@Scope("prototype")//多例作用域，每次从spring请求类的实例都会创建新的对象
//@Scope("singleton")//默认作用域单例作用域
//@Lazy//延迟加载，用时在创建
@Component
public  class ObjectPool {
    public ObjectPool(){
        System.out.println("objectPool()");
    }
    @PostConstruct//生命周期初始化，构造方法之后执行，可以在方法内部作用一些初始化
    public void init(){
        System.out.println("init()");
    }
    @PreDestroy//生命周期销毁方法，对象销毁之前执行，可以在对象销毁前做一些次元释放操作
    public void desotry(){
        System.out.println("desotry()");
    }
}
