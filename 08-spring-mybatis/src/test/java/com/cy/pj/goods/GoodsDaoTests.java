package com.cy.pj.goods;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
public class GoodsDaoTests {
    @Autowired
    private dao dao;
    @Test
    void testGoodsDao(){
        List<Goods> goods = dao.findGoods();
        for (Goods go:goods
             ) {
            System.out.println(go);

        }
    }
}
