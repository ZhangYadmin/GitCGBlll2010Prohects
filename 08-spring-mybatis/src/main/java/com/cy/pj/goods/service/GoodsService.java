package com.cy.pj.goods.service;

import com.cy.pj.goods.Goods;

import java.util.List;
public interface GoodsService {
   // @Select("select * from tb_goods")
    List<Goods> findGoods();
}
