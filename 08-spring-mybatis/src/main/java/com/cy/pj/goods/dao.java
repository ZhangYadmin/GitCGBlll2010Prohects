package com.cy.pj.goods;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 商品模块数据逻辑接口定义，在此就口中
 * 定义商品
 */
@Mapper
public interface dao {
   //@Select("select * from tb_goods")
    List<Goods> findGoods();
}
