package com.cy.pj.goods.service.impl;

import com.cy.pj.goods.Goods;
import com.cy.pj.goods.dao;
import com.cy.pj.goods.service.GoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * 商品业务逻辑对象，此对象中要实现
 * 核心业务
 * 拓展业务（日志，缓存，校验，权限，事务。。。）
 *
 */
@Service
public class GoodsServicelmpl implements GoodsService {
    //获取一个日志对象（推荐使用酸辣粉j包中的日志对象-因为这是规范）
    private static final Logger log
            =  LoggerFactory.getLogger(GoodsServicelmpl.class);
    @Autowired
    private dao dao;

    @Override
    public List<Goods> findGoods() {
        long t1=System.currentTimeMillis();
        //System.out.println("start"+t1);
        log.info("start:{}",t1);
        List<Goods> list =dao.findGoods();
        long t2=System.currentTimeMillis();
        log.info("end:{}",t2);
        //System.out.println("end" + t2);
        return list;
    }
}
