package com.cy.pj.goods;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

//@Repository
public class GoodsDaoImlp implements  dao{
    @Autowired
    private SqlSession sqlSession;
    @Override
    public List<Goods> findGoods() {
        //mybatis 数据访问实现
        //String statemaent="com.cy.pj.goods.dao.findGoods";//映射的namespace+elemented
        String namespace=this.getClass().getInterfaces()[0].getName();
        String elementId=Thread.currentThread().getStackTrace()[1].getMethodName();
        String statemaent=namespace+"."+elementId;
        List<Goods> list = sqlSession.selectList(statemaent);
        return list;
    }
}
