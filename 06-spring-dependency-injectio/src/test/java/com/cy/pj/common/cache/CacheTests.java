package com.cy.pj.common.cache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import sun.dc.pr.PRError;

@SpringBootTest
public class CacheTests {
//has a(是耦合吗？-是)
    @Autowired
    @Qualifier("softCache")//需要配置Autowire注解使用，不能单独使用，用于指定要注入的bean
    private Cache cache;
    @Test
    void testCache() {
           System.out.println(cache);
    }
}
